package com.r6lab.di;

public class InstanceHolder {
    private final Object instance;
    private final String name;

    private InstanceHolder(Object instance, String name) {
        this.instance = instance;
        this.name = name;
    }

    public Object getInstance() {
        return instance;
    }

    public String getName() {
        return name;
    }

    public static InstanceHolder of(Object instance) {
        return new InstanceHolder(instance, null);
    }


    public static InstanceHolder of(Object instance, String name) {
        return new InstanceHolder(instance, name);
    }
}

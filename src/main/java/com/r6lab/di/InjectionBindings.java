package com.r6lab.di;

public interface InjectionBindings {
    void configure(Context context);
}

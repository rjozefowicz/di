package com.r6lab.di;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
    /**
     * Specify injectable by name. This field is ignored for Collections
     * @return
     */
    String qualifier() default "";
    boolean nullable() default true;
}

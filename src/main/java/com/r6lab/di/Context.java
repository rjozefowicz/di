package com.r6lab.di;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Context {

    private final Map<Class<?>, List<InstanceHolder>> mappings = new HashMap<>();

    public final void bind(Object instance) {
        bind(instance, null);
    }

    public final void bind(Object instance, String name) {
        if (mappings.containsKey(instance.getClass())) {
            mappings.get(instance.getClass()).add(InstanceHolder.of(instance, name));
        } else {
            List<InstanceHolder> instanceHolders = new ArrayList<>();
            instanceHolders.add(InstanceHolder.of(instance, name));
            mappings.put(instance.getClass(), instanceHolders);
        }
    }

    public Map<Class<?>, List<InstanceHolder>> getMappings() {
        return mappings;
    }
}

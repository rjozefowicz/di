package com.r6lab.di;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public final class Injector {

    private final Context context;

    private Injector(Context context) {
        this.context = context;
        context
                .getMappings()
                .values()
                .stream()
                .flatMap(instanceHolders -> instanceHolders.stream())
                .forEach(instanceHolder -> inject(instanceHolder.getInstance()));
    }

    public void inject(List<Object> instances) {
        instances.forEach(instance -> inject(instance));
    }

    /**
     * Injects to single object instance and its subinstances
     *
     * @param instance
     */
    public void inject(Object instance) {
        try {
            for (Field field : instance.getClass().getDeclaredFields()) {
                Inject fieldAnnotation = field.getAnnotation(Inject.class);
                if (fieldAnnotation != null) {
                    if (Collection.class.isAssignableFrom(field.getType())) {
                        injectCollection(instance, field, fieldAnnotation);
                    } else {
                        injectSingleInstance(instance, field, fieldAnnotation);
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Injection exception. " + e.getLocalizedMessage());
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void injectCollection(Object instance, Field field, Inject fieldAnnotation) throws IllegalAccessException {
        Class<?> genericCollectionType = getCollectionGenericClass(field);

        Collection<Object> injectablesCollection = getCollectionImplementation(field.getType());
        List<Object> injectables = getInjectables(genericCollectionType);

        if (injectables.isEmpty() && !fieldAnnotation.nullable()) {
            throw new IllegalStateException("Missing binding for class " + field.getType());
        }
        injectablesCollection.addAll(injectables);
        setInjectable(instance, field, injectablesCollection);
    }

    private void injectSingleInstance(Object instance, Field field, Inject fieldAnnotation) throws IllegalAccessException {
        Object injectable = injectableObject(field, fieldAnnotation);
        setInjectable(instance, field, injectable);
    }

    private Class<?> getCollectionGenericClass(Field field) {
        try {
            ParameterizedType parametrizedListType = (ParameterizedType) field.getGenericType();
            return (Class<?>) parametrizedListType.getActualTypeArguments()[0];
        } catch (Exception e) {
            throw new IllegalStateException("Collection class must provide generic type for field " + field.getName());
        }
    }

    private List<Object> getInjectables(Class<?> genericCollectionType) {
        if (genericCollectionType.isInterface()) {
            return context
                    .getMappings()
                    .entrySet()
                    .stream()
                    .filter(e -> genericCollectionType.isAssignableFrom(e.getKey()))
                    .flatMap(e -> e.getValue().stream())
                    .map(instanceHolder -> instanceHolder.getInstance())
                    .collect(Collectors.toList());

        } else {
            return context.getMappings().getOrDefault(genericCollectionType, new ArrayList<>()).stream()
                    .map(instanceHolder -> instanceHolder.getInstance())
                    .collect(Collectors.toList());
        }
    }

    private Collection<Object> getCollectionImplementation(Class<?> collectionClass) {

        if (List.class.isAssignableFrom(collectionClass)) {
            return new ArrayList<>();
        } else if (Set.class.isAssignableFrom(collectionClass)) {
            return new HashSet<>();
        } else if (Queue.class.isAssignableFrom(collectionClass)) {
            new LinkedBlockingQueue<>();
        }
        throw new IllegalStateException("Unsupported collection class " + collectionClass);

    }

    private void setInjectable(Object instance, Field field, Object injectable) throws IllegalAccessException {
        boolean accessible = field.isAccessible();
        if (!accessible) {
            field.setAccessible(!accessible);
        }
        field.set(instance, injectable);
        field.setAccessible(accessible);
    }

    private Object injectableObject(Field field, Inject fieldAnnotation) {
        List<InstanceHolder> instanceHolders = context
                .getMappings()
                .entrySet()
                .stream()
                .filter(e -> field.getType().isAssignableFrom(e.getKey()))
                .flatMap(e -> e.getValue().stream())
                .collect(Collectors.toList());

        if (instanceHolders.isEmpty() && !fieldAnnotation.nullable()) {
            throw new IllegalStateException("Missing binding for class " + field.getType());
        }

        if (!fieldAnnotation.qualifier().isEmpty()) {
            Optional<Object> injectable = instanceHolders
                    .stream()
                    .filter(instanceHolder -> instanceHolder.getName().equals(fieldAnnotation.qualifier()))
                    .map(instanceHolder -> instanceHolder.getInstance())
                    .findAny();
            if (injectable.isPresent()) {
                return injectable.get();
            } else if (!fieldAnnotation.nullable()) {
                throw new IllegalStateException("Missing binding for class " + field.getType());
            }
            return null;
        } else if (instanceHolders.size() > 1) {
            throw new IllegalStateException("More than one instance available to inject for class " + field.getType());
        }
        return instanceHolders.stream().map(instanceHolder -> instanceHolder.getInstance()).findFirst().orElse(null);

    }

    public static InjectorBuilder builder() {
        return new InjectorBuilder();
    }

    public static class InjectorBuilder {
        private Context context = new Context();

        public InjectorBuilder withBindings(InjectionBindings configuration) {
            configuration.configure(context);
            return this;
        }

        public Injector build() {
            return new Injector(context);
        }

    }

}

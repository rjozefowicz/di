package com.r6lab.di.repository;

import java.util.List;

public interface Repository<E> {
    void persist(E entity) throws PersistenceException;
    List<E> findAll();
}

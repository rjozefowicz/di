package com.r6lab.di.repository;

import com.r6lab.di.Inject;
import com.r6lab.di.model.PersistableEntity;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class JdbcRepository implements Repository<PersistableEntity> {

    private List<PersistableEntity> entities = new ArrayList<>();

    @Inject
    private DataSource dataSource;

    @Override
    public void persist(PersistableEntity entity) throws PersistenceException {
        System.out.println("Persisting via JDBC connector " + dataSource.hashCode());
        entities.add(entity);
    }

    @Override
    public List<PersistableEntity > findAll() {
        return entities;
    }
}

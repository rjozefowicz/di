package com.r6lab.di.repository;

import com.r6lab.di.Inject;
import com.r6lab.di.model.PersistableEntity;

import java.util.List;

public class LocalFileRepository implements Repository<PersistableEntity> {

    @Inject(qualifier = "repositoryLocation")
    private String directory;

    @Override
    public void persist(PersistableEntity entity) {
        System.out.println("Persisting in " + directory);
    }

    @Override
    public List<PersistableEntity> findAll() {
        return null;
    }
}

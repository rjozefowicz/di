package com.r6lab.di;

import com.r6lab.di.model.PersistableEntity;
import com.r6lab.di.repository.JdbcRepository;
import com.r6lab.di.repository.LocalFileRepository;
import com.r6lab.di.service.JdbcLogicService;
import com.r6lab.di.service.PersistableEntityValidator;
import com.r6lab.di.service.PersistanceService;
import org.mockito.Mockito;

import javax.sql.DataSource;
import java.util.stream.IntStream;

public class Application {

    @Inject
    private PersistanceService persistanceService;

    @Inject
    private JdbcLogicService jdbcLogicService;

    public void start() {
        System.out.println("Persist in multiple repositories...");
        IntStream.range(0, 10).boxed().forEach(integer -> {
            PersistableEntity entity = new PersistableEntity();
            entity.setValue(String.valueOf(integer));
            persistanceService.validateAndSave(entity);
        });

        System.out.println("Print all entities from JDBC repository...");
        jdbcLogicService.printAll();
    }

    public static void main(String[] args) {

        Application application = new Application();

        Injector
                .builder()
                .withBindings(bindings -> {
                    // instances
                    bindings.bind(new PersistableEntityValidator());
                    bindings.bind(new PersistanceService());
                    bindings.bind(new JdbcLogicService());
                    bindings.bind(Mockito.mock(DataSource.class));
                    bindings.bind(new JdbcRepository());
                    bindings.bind(new LocalFileRepository());
                })
                .withBindings(bindings -> {
                    // values
                    bindings.bind(new String("/tmp"), "repositoryLocation");
                })
                .build()
                .inject(application);

        application.start();
    }

}

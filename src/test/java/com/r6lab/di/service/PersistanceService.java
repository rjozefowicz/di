package com.r6lab.di.service;

import com.r6lab.di.Inject;
import com.r6lab.di.model.PersistableEntity;
import com.r6lab.di.repository.Repository;

import java.util.List;

public class PersistanceService {

    @Inject
    private List<Repository> repositories;

    @Inject
    private Validator<PersistableEntity> validator;

    public void validateAndSave(PersistableEntity entity) {
        if (validator.validate(entity)) {
            repositories.forEach(repository -> repository.persist(entity));
        }
    }

}

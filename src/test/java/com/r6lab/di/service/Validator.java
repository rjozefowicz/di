package com.r6lab.di.service;

public interface Validator<E> {
    boolean validate(E entity);
}

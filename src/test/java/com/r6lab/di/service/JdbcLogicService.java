package com.r6lab.di.service;

import com.r6lab.di.Inject;
import com.r6lab.di.repository.JdbcRepository;

public class JdbcLogicService {

    @Inject
    private JdbcRepository jdbcRepository;

    public void printAll() {
        jdbcRepository.findAll().forEach(entity -> System.out.println(entity.getValue()));
    }

}

package com.r6lab.di.model;

public class PersistableEntity {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
